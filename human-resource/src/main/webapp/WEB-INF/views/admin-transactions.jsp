<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>transactions</title>
		
		<style type="text/css">
			*{
				margin:0;
				padding:0;
			}
			.navbar{
				width:100%;
				height: 46px;
				background : #5f5f5f;
				
			}
			.navbar-links{
				display: flex;
				justify-content: space-around;
				text-align: center;
			}
			.navbar-links li {
				list-style: none;
				padding:12px;
			}
			.navbar-links li a{
				text-decoration: none;
				color: azure;
				padding:12px;
				font-size: 18px;
			}
			.navbar-links li:hover{
				background: black; 
			}			
			.transactions-table{
				width: 55%;
			}
			.transactions-table tr td{
				text-align: center;
				 vertical-align: middle;
			}
		
			.transaction-table-container{
				margin-top:20px;
				display: flex;
				justify-content: center;
				margin-bottom:20px;
			}
			.transactions-table{
			    border-collapse: collapse;
		    }
		    .transactions-table th,.transactions-table td {
		    	border: 1px solid #ccc;
		    	padding: 10px;
		   	 	text-align: left;
		    }
		  	tr:nth-child(even) {
		    	background-color: #eee;
		  	}
		  	tr:nth-child(odd) {
		    	background-color: #fff;
		  	}     
		  	h3{
		  		margin:20px;
		  		text-align: center;
		  	}       
			 input[type=text] {
			    padding:5px; 
			    border:2px solid #ccc; 
			    -webkit-border-radius: 5px;
			    border-radius: 5px;
			}
			.transactions-form-container{
				display: flex;
				justify-content: center;
			}
		</style>
	</head>
	<body>
		<div class="navbar">
			<ul class="navbar-links">
				<li>
					<a href="payments">Payments</a>
				</li>
				
				<li>
					<a href="users">Users</a>		
				</li>
				
				<li>
					<a href="transactions">Transactions</a>		
				</li>
				
				<li>
					<a href="/human-resource/logoutProcess">Logout</a>		
				</li>
			</ul>
		</div>
		<h3>Here you can find all transactions assigned to users</h3>
		<div class="transaction-table-container">
			<table class="transactions-table" border="1">
				<tr>
					<th>TransactionId</th>
					<th>Date</th>
					<th>Status</th>
					<th>Payment User</th>
					<th>Amount</th>
				</tr>
				
				<c:if test="${transactions != null}">
					<c:forEach items="${transactions}" var="transaction">
						<tr>
							<td>${transaction.transactionId}</td>
							<td>${transaction.transactionDate}</td>
							<td>${transaction.transactionStatus}</td>
							<td>${transaction.payment.user}</td>
							<td>${transaction.payment.amount} &euro;</td>
						</tr>
					</c:forEach>
				</c:if>
			</table>
		</div>
	</body>
</html>