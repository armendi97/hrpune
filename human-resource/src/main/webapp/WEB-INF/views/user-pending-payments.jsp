<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!Doctype html>

<html>
<head>
		<meta charset="ISO-8859-1">
		<title>Pending payments</title>
		
		<style type="text/css">
			*{
				margin:0;
				padding:0;
			}
			.navbar{
				height: 46px;
				background : #5f5f5f;
				
			}
			.navbar-links{
				display: flex;
				justify-content: space-around;
				text-align: center;
			}
			.navbar-links li {
				list-style: none;
				padding:12px;
			}
			.navbar-links li a{
				text-decoration: none;
				color: azure;
				padding:12px;
				font-size: 18px;
			}
			.navbar-links li:hover{
				background: black; 
			}
			.pendingpayment{
				width: 60%;
				margin-left: 1%;
			}
			h3{
		  		margin:20px;
		  		text-align: center;
		  	}  
			.pendingpayment tr td{
				text-align: center;
				vertical-align: middle;
			}
			.admin-payments-header{
			    padding-top:1%;
				background : #5f5f5f;
				height: 46px;
				color:azure;
				text-align: center;
			}
			.admin-payments-header h2{
				
				text-align: center;
				/* margin-left:30%;  */
				padding: 4px;
			}
			.pendingpayment{
			    border-collapse: collapse;
		    }
		    th, td {
		    	border: 1px solid #ccc;
		    	padding: 10px;
		   	 	text-align: left;
		    }
		  	tr:nth-child(even) {
		    	background-color: #eee;
		  	}
		  	tr:nth-child(odd) {
		    	background-color: #fff;
		  	} 
		  	.table-container{
		  		display:flex;
				justify-content: center;
			}
			
		</style>
</head>
	<body>
		<div class="navbar">
			<ul class="navbar-links">
				<li>
					<a href="allpayments">All payments</a>
				</li>
			
				<li>
					<a href="pendingpayments">Pending payments</a>
				</li>
				
				<li>
					<a href="/human-resource/logoutProcess">Logout</a>		
				</li> 
			</ul>
		</div>
		<h3>All pending payments</h3>
		<div class="table-container">
		<table class="pendingpayment"border="1">
			<tr>
				<th>Amount</th>
				<th>Owner</th>
				<th>Payment Description</th>
				<th>Action</th>
			</tr>
			<c:if test="${payments != null}">
			<c:forEach items="${payments}" var="payment">
					<c:url value="declinePayment" var="decline">
							<c:param name="id" value="${payment.paymentId}"></c:param>
					</c:url>
	
					<tr>
						<td>${payment.amount}</td>
						<td>${payment.owner.username}</td>
						<td>${payment.paymentDescription}</td>
						<c:url value="cardPayment" var="pay">
						<c:param name="paymentId" value="${payment.paymentId}"></c:param>
						</c:url>
						<td><a href="${pay}">Pay</a>   <a href="${decline}">Decline</a></td>
					</tr>
				</c:forEach>
			</c:if>	
		</table>
		</div>
	
	</body> 
</html>