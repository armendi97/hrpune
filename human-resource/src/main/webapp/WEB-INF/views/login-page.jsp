<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
	<title>Login - Page</title>
	<style type="text/css">
		body {
			text-align: center;
		}
		*{
			margin:0;
				padding:0;
		}
		
		form {
			display: inline-block;
			text-align: center;
			
		}
		.navbar{
				height: 46px;
				background : #5f5f5f;	
		}
		.navbar-links{
				display: flex;
				justify-content: space-around;
				text-align: center;
				margin:0;
				padding:0;
		}
		.navbar-links li {
				list-style: none;
				padding:10px;
		}
		.navbar-links li a{
				font-family: fantasy;
				text-decoration: none;
				color: azure;
				padding:10px;
				font-size: 22px;
		}
		.error{
			color:red;
			margin-bottom: 0;
			margin-top: 10px;
		}	
		input[type=submit] {
			
			    padding:7px 20px; 
			    background:#ccc; 
			    border:0 none;
			    cursor:pointer;
			    -webkit-border-radius: 5px;
			    border-radius: 5px; 
		}

	</style>
</head>
<body>
	<div class="navbar">
		<ul class="navbar-links">
			<li>
				<a href="">Welcome to Payments</a>
			</li>
			
		</ul>
	</div>
	<br>
	<br>
	<h4>LOGIN :</h4>
	<br>
	<form:form action="loginProcess" modelAttribute="login" method="POST">
	
			<b>Username</b> : <form:input path="username" />
		<br><br>
			<b>Password</b> : <form:password path="password" />
			
		<br>
		<p class="error"><i>${invalid}</i></p>
		<c:if test="${error == true}">
			<p class="error"><i>Invalid username or password.</i></p>
		</c:if>
		<c:if test="${logout == true}">
			<p class="logout"><i>You have been logged out.</i></p>
		</c:if>
		<br>
		<input type="submit" value="Submit" />
	</form:form>
	
</body>
</html>