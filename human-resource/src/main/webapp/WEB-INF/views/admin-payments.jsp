<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Payments</title>
		
		<style type="text/css">
			*{
				margin:0;
				padding:0;
			}
			.navbar{
				width:100%;
				height: 46px;
				background : #5f5f5f;
				
			}
			.navbar-links{
				display: flex;
				justify-content: space-around;
				text-align: center;
			}
			.navbar-links li {
				list-style: none;
				padding:12px;
			}
			.navbar-links li a{
				text-decoration: none;
				color: azure;
				padding:12px;
				font-size: 18px;
			}
			.navbar-links li:hover{
				background: black; 
			}			
			.payments-table{
				width: 55%;
			}
			.payments-table tr td{
				text-align: center;
				 vertical-align: middle;
			}
		
			.payment-table-container{
				margin-top:20px;
				display: flex;
				justify-content: center;
				margin-bottom:20px;
			}
			.payments-table{
			    border-collapse: collapse;
		    }
		    .payments-table th,.payments-table td {
		    	border: 1px solid #ccc;
		    	padding: 10px;
		   	 	text-align: left;
		    }
		  	tr:nth-child(even) {
		    	background-color: #eee;
		  	}
		  	tr:nth-child(odd) {
		    	background-color: #fff;
		  	}     
		  	h3{
		  		margin:20px;
		  		text-align: center;
		  	}       
			.button-anchor,input[type=submit]{
				padding:7px 20px; 
			    background:#ccc; 
			    border:0 none;
			    cursor:pointer;
			    -webkit-border-radius: 5px;
			    border-radius: 5px; 
			    text-decoration: none;
			    color: azure;
			 }
			 .button-anchor-container{
			 	display: flex;
			 	justify-content: space-around;
			 }
			 input[type=text] {
			    padding:5px; 
			    border:2px solid #ccc; 
			    -webkit-border-radius: 5px;
			    border-radius: 5px;
			}
			.payments-form-container{
				display: flex;
				justify-content: center;
			}
		</style>
	</head>
	<body>
		<div class="navbar">
			<ul class="navbar-links">
				<li>
					<a href="payments">Payments</a>
				</li>
				
				<li>
					<a href="users">Users</a>		
				</li>
				
				<li>
					<a href="transactions">Transactions</a>		
				</li>
				
				<li>
					<a href="/human-resource/logoutProcess">Logout</a>		
				</li>
			</ul>
		</div>
		<h3>Here you can find all payments assigned to users</h3>
			<%-- <div class="payments-form-container">
					<form action="payments" class="payments-form"> 
						<table class="search-table">
							<tr>
								<td>
									<input type="text" name="username" placeholder="username">
								</td>
					
								<td>
									<input type="text" name="amount" placeholder="amount">
								</td>
						
								<td>
									<input type="text" name="paymentDescription" placeholder=description>
								</td>
							
								<td>
									<input type="text" name="status" placeholder="status">
								</td>
							
								<td>
									<input type="submit" name="Submit">
								</td>
							</tr>	
						</table>
					</form>
			</div>
			<br> --%>
			<div class="button-anchor-container">
				<a class="button-anchor" href="addpayment">Add payment</a>
			</div>
		<div class="payment-table-container">
			<table class="payments-table" border="1">
				<tr>
					<th>PaymentId</th>
					<th>Amount</th>
					<th>Description</th>
					<th>User</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
				
				<c:if test="${payments != null}">
					<c:forEach items="${payments}" var="payment">
						<tr>
							<td>${payment.paymentId}</td>
							<td>${payment.amount}&euro;</td>
							<td>${payment.paymentDescription}</td>
							<td>${payment.user.username}</td>
							<td>${payment.status}</td>
							<c:if test="${payment.status == 'PENDING'}">
								<c:url value="deletePayment" var="delete" >
									<c:param name="paymentId" value="${payment.paymentId}"></c:param>
								</c:url>
								<c:url value="editPayment" var="edit">
									<c:param name="paymentId" value="${payment.paymentId}"></c:param>
								</c:url>
								<td><a href="${edit}">edit</a>    <a href="${delete}" onclick="return confirm('Are u sure u want to delete this payment');">delete</a></td>
								
							</c:if>
							<c:if test="${payment.status != 'PENDING'}">
								<td></td>
							</c:if>
						</tr>
					</c:forEach>
				</c:if>
			</table>
		</div>
	</body>
</html>