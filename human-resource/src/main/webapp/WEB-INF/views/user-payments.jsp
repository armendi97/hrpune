<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
	<head>	
		<meta charset="ISO-8859-1">
		<title>All Payments</title>
		
		<style type="text/css">
			.payments-table{
			    border-collapse: collapse;
		    }
		    th, td {
		    	border: 1px solid #ccc;
		    	padding: 10px;
		   	 	text-align: left;
		    }
		  	tr:nth-child(even) {
		    	background-color: #eee;
		  	}
		  	tr:nth-child(odd) {
		    	background-color: #fff;
		  	}       
		  	*{
				margin:0;
				padding:0;
			}
			.payments-table{
				margin:10px;
				width: 60%;
				
			}
			.payments-table tr td{
				text-align: center;
				vertical-align: middle;
			}
			.admin-payments-header{
				background : #5f5f5f;
				height: 46px;
				color:azure;
				padding-top:5px;
			}
			.admin-payments-header h2{
				text-align: center;
				padding-top:5px;
				
			}
			.payments-table-container{
				display:flex;
				justify-content: center;
			}
			.navbar{
				height: 46px;
				background : #5f5f5f;
				
			}
			h3{
		  		margin:20px;
		  		text-align: center;
		  	}   
			.navbar-links{
				display: flex;
				justify-content: space-around;
				text-align: center;
			}
			.navbar-links li {
				list-style: none;
				padding:12px;
			}
			.navbar-links li a{
				text-decoration: none;
				color: azure;
				padding:12px;
				font-size: 18px;
			}
			.navbar-links li:hover{
				background: black; 
			}
			 
		</style>
</head>
	<body>
		
		<div class="navbar">
			<ul class="navbar-links">
				<li>
					<a href="allpayments">All payments</a>
				</li>
			
				<li>
					<a href="pendingpayments">Pending payments</a>
				</li>
				
				<li>
					<a href="/human-resource/logoutProcess">Logout</a>	
				</li> 
			</ul>
		</div>
		<h3>Here you can find all payments that were assigned to you</h3>
		<h3 style = "color:#4CAF50;">${transactionResponse}</h3>
		<div class="payments-table-container">
			<table class="payments-table" border="1">
				<tr>
					<th>Payment ID</th>
					<th>Payment Description</th>
					<th>Amount</th>
					<th>Status</th>
					<th>Username</th>

				</tr>
				<c:forEach items="${payments}" var="payment">
					<tr>
						<td>${payment.paymentId}</td>
						<td>${payment.paymentDescription}</td>
						<td>${payment.amount} &euro;</td>
						<td>${payment.status}</td>
						<td>${payment.owner.username}</td>
					
					</tr>
				</c:forEach>
			</table>
		</div>
	</body> 
</html>