<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
	<%@ page isELIgnored="false"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Insert title here</title>
		<style type="text/css">
			*{
				margin:0;
				padding:0;
			}
			html,body{
				height: 100%;
			}
			.payments-table{
				width: 55%;
			}
			.navbar{
				height: 46px;
				background : #5f5f5f;
				
			}
			.navbar-links{
				display: flex;
				justify-content: space-around;
				text-align: center;
			}
			.navbar-links li {
				list-style: none;
				padding:12px;
			}
			.navbar-links li a{
				text-decoration: none;
				color: azure;
				padding:12px;
				font-size: 18px;
			}
			.navbar-links li:hover{
				background: black; 
			}
			.add-payment-form{
				display:flex;
				justify-content:center;
				margin-top: 8%;
			}
			table{
				width: 19%;
			}
			table tr{
				height: 50px;
			}
			.main-form{
				width: 20%;
			}
			.main-form form{
				width: 100%;
			}
			.main-form table{
				width: 100%;
			}
			input[type=text] {
				width:100%;
			    padding:5px; 
			    border:2px solid #ccc; 
			    -webkit-border-radius: 5px;
			    border-radius: 5px;
			}
			textarea{
				width:100%;
				padding:4px; 
			    border:2px solid #ccc; 
			    -webkit-border-radius: 5px;
			    border-radius: 5px;
			}
			input[type=submit] {
			
			    padding:7px 20px; 
			    background:#ccc; 
			    border:0 none;
			    cursor:pointer;
			    -webkit-border-radius: 5px;
			    border-radius: 5px; 
			}
			select{
				width:100%;
				padding:4px; 
			    border:2px solid #ccc; 
			    -webkit-border-radius: 5px;
			    border-radius: 5px;
			}
		</style>
	</head>
	<body>
		<div class="navbar">
			<ul class="navbar-links">
				<li>
					<a href="payments">Payments</a>
				</li>
				
				<li>
					<a href="users">Users</a>		
				</li>
				
				<li>
					<a href="transactions">Transactions</a>		
				</li>
				
				<li>
					<a href="/human-resource/logoutProcess">Logout</a>		
				</li>
			</ul>
		</div>
		
		<div class="add-payment-form">
		 	<div class="main-form">
				<form:form action="processAddPayment" method="post" modelAttribute="newpayment" name="myForm" onsubmit="return(validate());">
					<table>
						<tr>
							<td>
								Payment Amount:
							</td>
							<td>
								<form:input name="amount" path="amount"/> 
								<form:errors path="amount"/>
							</td>
						</tr>
						<tr>
							<td>
								PaymentDescription:
							</td>
							<td>
								<form:textarea id="txtarea_consolidation" path="paymentDescription"/>
								<form:errors path="paymentDescription"/>
							</td>
						</tr>
						<tr>
							<td>
								Select User:
							</td>
							<td>
								<select name="username" path="username">
									<option value="none">Select User</option>
									<c:forEach items="${users}" var="user">
										<option value="${user.username}">${user}</option>
									</c:forEach>
								</select>
								${error}
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="submit" value="Save">
							</td>
						</tr>
					</table>
				</form:form>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		function validate(){
					
			if(isNaN(document.myForm.amount.value)){
				alert("Amount should be a number");
				document.myForm.amount.focus();
				return false;
			}
			
			if(document.myForm.amount.value < 1 ){
				alert("Amount cannot be less than 1");
				document.myForm.amount.focus();
				return false;
			}
			
			if(document.myForm.amount.value == "" ){
				alert("Amount cannot be empty");
				document.myForm.amount.focus();
				return false;
			}
			
			if (document.getElementById("txtarea_consolidation").value == "" ){
                alert("Description cannot be empty");
                document.getElementById("txtarea_consolidation").focus();
            	return false;
			}
			
			if (document.getElementById("txtarea_consolidation").value.length < 5 ){
                alert("Description must at least contan 5 characters");
                document.getElementById("txtarea_consolidation").focus();
            	return false;
			}
		
			if(document.myForm.username.value == "none" ) {
	            alert( "Please select a user" );
	            return false;
	         }
					
		}
	</script>
</html>