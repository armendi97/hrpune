<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
	<%@ page isELIgnored="false"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>New User</title>
		<style type="text/css">
			*{
				margin:0;
				padding:0;
			}
			html,body{
				height: 100%;
			}
			.payments-table{
				width: 55%;
			}
			.navbar{
				height: 46px;
				background : #5f5f5f;
				
			}
			.navbar-links{
				display: flex;
				justify-content: space-around;
				text-align: center;
			}
			.navbar-links li {
				list-style: none;
				padding:12px;
			}
			.navbar-links li a{
				text-decoration: none;
				color: azure;
				padding:12px;
				font-size: 18px;
			}
			.navbar-links li:hover{
				background: black; 
			}
			.add-payment-form{
				margin-top:80px;
				display: flex;
				justify-content: center;
			}
			table tr{
				height: 50px;
			}
			input[type=text], input[type=password] {
			    padding:5px; 
			    border:2px solid #ccc; 
			    -webkit-border-radius: 5px;
			    border-radius: 5px;
			}
			textarea{
				width:100%;
				padding:4px; 
			    border:2px solid #ccc; 
			    -webkit-border-radius: 5px;
			    border-radius: 5px;
			}
			input[type=submit] {
			    padding:7px 20px; 
			    background:#ccc; 
			    border:0 none;
			    cursor:pointer;
			    -webkit-border-radius: 5px;
			    border-radius: 5px; 
			}
			select{
				padding:4px; 
			    border:2px solid #ccc; 
			    -webkit-border-radius: 5px;
			    border-radius: 5px;
			}
			.error{
				color:red;
			}
		</style>
	</head>
	<body>
		<div class="navbar">
			<ul class="navbar-links">
				<li>
					<a href="payments">Payments</a>
				</li>
				
				<li>
					<a href="users">Users</a>		
				</li>
				
				<li>
					<a href="transactions">Transactions</a>		
				</li>
				
				<li>
					<a href="/human-resource/logoutProcess">Logout</a>		
				</li>
			</ul>
		</div>
		
		<div class="add-payment-form">
			
			<form:form action="processAddUser" method="post" modelAttribute="newuser" name="myForm" onsubmit="return(validate());">
				<table>
					<tr>
						<td>
							Email:
						</td>
						<td>
							<form:input name="email" path="email"/> <br>
							<form:errors class="error" path="email"/>
						</td>
					</tr>
					<tr>
						<td>
							Username:
						</td>
						<td>
							<form:input name="username" path="username"/> <br>
							<form:errors  class="error" path="username"/> 
						</td>
					</tr>
					<tr>
						<td>
							Password:
						</td>
						<td>
							<form:password name="password" path="password"/> <br>
							<form:errors class="error" path="password"/>
						</td>
					</tr>
					<%-- <tr>
						<td>
							Select User:
						</td>
						<td>
							<select name="username">
							<option value="">PleaseChoose</option>
								<c:forEach items="${users}" var="user">
									<option value="${user.username}">${user}</option>
								</c:forEach>
							</select>
						</td>
					</tr> --%>
					<tr>
						<td>
						</td>
						<td>
							<input type="submit" value="Save">
						</td>
					</tr>
				</table>
			</form:form>
		
		</div>
		
	</body>

</html>