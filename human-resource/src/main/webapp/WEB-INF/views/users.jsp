<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<!Doctype html>

<html>
<head>
	<style type="text/css">
			*{
				margin:0;
				padding:0;
			}
			.allUsers{
				width: 55%;
			}
			.allUsers tr td{
				text-align: center;
				 vertical-align: middle;
			}
			.navbar{
				height: 46px;
				background : #5f5f5f;
				
			}
			.navbar-links{
				display: flex;
				justify-content: space-around;
				text-align: center;
			}
			.navbar-links li {
				list-style: none;
				padding:12px;
			}
			.navbar-links li a{
				text-decoration: none;
				color: azure;
				padding:12px;
				font-size: 18px;
			}
			.navbar-links li:hover{
				background: black; 
			}
			.user-table-container{
				margin-top:40px;
				display: flex;
				justify-content: center;
			}
			.newUser{
				text-align: center;
				margin-top:40px;
			}
			.newUser a b{
				color:azure;
			}
			table {
			    border-collapse: collapse;
		    }
		    th, td {
		    	border: 1px solid #ccc;
		    	padding: 10px;
		   	 	text-align: center;
		    }
		  	tr:nth-child(even) {
		    	background-color: #eee;
		  	}
		  	tr:nth-child(odd) {
		    	background-color: #fff;
		  	}  
		  	button {
			    padding:7px 20px; 
			    background:#ccc; 
			    border:0 none;
			    cursor:pointer;
			    -webkit-border-radius: 5px;
			    border-radius: 5px; 
			}       
		</style>
</head>
	<body>
		
		
		
		<div class="navbar">
			<ul class="navbar-links">
				<li>
					<a href="payments">Payments</a>
				</li>
				
				<li>
					<a href="users">Users</a>		
				</li>
				
				<li>
					<a href="transactions">Transactions</a>		
				</li>
				
				<li>
					<a href="/human-resource/logoutProcess">Logout</a>		
				</li>
			</ul>
		</div>
		<div class="newUser">
			<a href="adduser"><button><b>NEW USER</b></button></a>
		</div>
		<div class="user-table-container">
		<table class="allUsers" border="1">
			<tr>
				<th>User ID</th>
				<th>Username</th>
				<th>Email</th>
				<th>Delete</th>
			</tr>
			<c:forEach items="${payments}" var="user" >
				<tr>
					<td>${user.userId}</td>
					<td>${user.username}</td>
					<td>${user.email}</td>
					
					<c:url value="processDeleteUser" var="delete">
						<c:param name="userId" value="${user.userId}"></c:param>
					</c:url>
					<td><a href="${delete}">Delete</a></td>
				</tr>
			</c:forEach>
			
		</table>
		</div>
	</body> 
</html>