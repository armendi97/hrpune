<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>	
		<meta charset="ISO-8859-1">
		<title>Admin Dashboard</title>
		<style type="text/css">
			html, body {
 			 	height: 100%;
			}	
			*{
				margin:0;
				padding:0;
			}
			.navbar{
				height: 46px;
				background : #5f5f5f;
				
			}
			.navbar-links{
				display: flex;
				justify-content: space-around;
				text-align: center;
			}
			.navbar-links li {
				list-style: none;
				padding:12px;
			}
			.navbar-links li a{
				text-decoration: none;
				color: azure;
				padding:12px;
				font-size: 18px;
			}
			.navbar-links li:hover{
				background: black; 
			}
			.main{
				width: 100%;
				height: 100%;
			}
			.main h1{
				text-align: center;
				line-height: 300px;
			}
			
			
		</style>		
		
	</head>
	<body>
	<div class="navbar">
		<ul class="navbar-links">
			<li>
				<a href="payments">Payments</a>
			</li>
			
			<li>
				<a href="users">Users</a>		
			</li>
			
			<li>
				<a href="transactions">Transactions</a>		
			</li>
			
			<li>
				<a href="/human-resource/logoutProcess">Logout</a>		
			</li>
		</ul>
	</div>

	
	<div class="main">
		<h1>Welcome To Admin Page</h1>
	</div>
	</body>
</html>