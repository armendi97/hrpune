package asseco.mvc.hibernate.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import asseco.mvc.hibernate.model.Payment;
import asseco.mvc.hibernate.model.Transaction;
import asseco.mvc.hibernate.model.User;
import asseco.mvc.hibernate.service.PaymentService;
import asseco.mvc.hibernate.service.TransactionService;
import asseco.mvc.hibernate.service.UserServices;
import asseco.mvc.hibernate.util.PaymentStatus;
import asseco.mvc.hibernate.util.TransactionStatus;

@Controller
@RequestMapping("user")
public class TransactionController {

	private String message;
	@Autowired
	private PaymentService paymentService;
	@Autowired
	private UserServices userService;
	@Autowired
	private TransactionService transactionService;

	@RequestMapping("/home")
	public String showTransactions(Model model, HttpServletRequest request) {
		return "user-dashboard";
	}

	@GetMapping("/allpayments")
	public String allpayments(Model model, HttpServletRequest request) {
		model.addAttribute("payments", paymentService.findByUser((User) request.getAttribute("user")));
		System.out.println(message);
		if(message == null || message.trim().isEmpty()) {
			request.removeAttribute("transactionResponse");
		}
		else {
			request.setAttribute("transactionResponse", message);
			message = "";
		}
		return "user-payments";
	}

	@GetMapping("/pendingpayments")
	public String pendingPayments(Model model, HttpServletRequest request) {
		List<Payment> payments = paymentService.findByUserAndStatus((User) request.getSession().getAttribute("user"),
				PaymentStatus.PENDING);
		model.addAttribute("payments", payments);
		return "user-pending-payments";
	}

	@GetMapping("/declinePayment")
	public String declinePayment(Model model, HttpServletRequest request, @RequestParam("id") long id) {
		System.out.println(id);
		Payment payment = paymentService.findById(id);
		payment.setStatus(PaymentStatus.DECLINED);
		paymentService.save(payment);
		return "redirect:/user/pendingpayments";
	}

	@PostMapping("/paypayment")
	public String addNewPayment(Payment payment, String username, HttpServletRequest request) {
		payment.setUser(userService.findByUsername(username));
		payment.setOwner((User) request.getSession().getAttribute("user"));
		paymentService.save(payment);
		return "user-add-payment";
	}

	@GetMapping("/cardPayment")
	public String cardPayment(@RequestParam("paymentId") long id, Model model) {
		Payment payment = paymentService.findById(id);
		model.addAttribute("payment", payment);
		return "card-payment";
	}

	@PostMapping("/processCardPayment")
	public String processCardPayment(@RequestParam("payment") long id, @RequestParam("cardname") String cname,
			@RequestParam("cardnumber") String cnum, @RequestParam("expmonth") String expmonth,
			@RequestParam("expyear") String expyear, @RequestParam("cvv") String cvv,
			@RequestParam("amount") String amount, HttpServletRequest session) {

		try {
			HttpResponse response = Request.Post("https://neon-app.asseco-see.com.tr/msu/api/v2")
					.addHeader("Accept", "application/json")
					.bodyForm(Form.form().add("ACTION", "SALE").add("MERCHANT", "juniorpayment")
							.add("MERCHANTUSER", "juniorpayment.api@yopmail.com").add("MERCHANTPASSWORD", "Panda123.")
							.add("SESSIONTYPE", "PAYMENTSESSION").add("AMOUNT", amount)
							.add("MERCHANTPAYMENTID", "LC-" + new Date().getTime()).add("CURRENCY", "EUR")
							.add("RETURNURL", "http://localhost:8090/human-resource/pendingpayments")
							.add("CUSTOMER", ((User) session.getAttribute("user")).getUsername())
							.add("CUSTOMERNAME", cname)
							.add("CUSTOMEREMAIL", ((User) session.getAttribute("user")).getEmail()).add("CARDPAN", cnum)
							.add("CARDEXPIRY", expmonth + "." + expyear).add("NAMEONCARD", cname).add("LANGUAGE", "EN")
							.build())
					.execute().returnResponse();
			String responseBody = EntityUtils.toString(response.getEntity(), "UTF-8");
			JSONParser parser = new JSONParser();
			JSONObject responseParsed = (JSONObject) parser.parse(responseBody);
			System.out.println(responseParsed.get("responseCode"));
			System.out.println(responseParsed.get("responseMsg"));
			Payment payment = paymentService.findById(id);
			Transaction transaction = new Transaction();
			transaction.setDate(new Date());
			transaction.setPayment(payment);
			if (responseParsed.get("responseCode").equals("00")) {
				payment.setStatus(PaymentStatus.APROVED);
				paymentService.save(payment);
				transaction.setTransactionStatus(TransactionStatus.APPROVED);
				message = "Transaction was successfull";
			} else {
				transaction.setTransactionStatus(TransactionStatus.DENIED);
				message = "Transaction failed";
			}
			transactionService.addTransaction(transaction);
		} catch (IOException | ParseException e) {
			//
		}

		return "redirect:/user/allpayments";
	}

}
