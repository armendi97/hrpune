package asseco.mvc.hibernate.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import asseco.mvc.hibernate.model.User;
import asseco.mvc.hibernate.service.UserServices;
import asseco.mvc.hibernate.util.Role;

@Controller
public class LoginController {
	@Autowired
	private UserServices userServices;

	@RequestMapping("/")
	public String home() {
		return "redirect:/login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView showLogin(HttpSession session) {
		ModelAndView mav;
		if (session.getAttribute("user") == null || ((User) session.getAttribute("user")).getRole() == Role.DELETED) {
			mav = new ModelAndView("login-page");
			mav.addObject("login", new User());
		} else {
			session.setAttribute("user", session.getAttribute("user"));
			if (((User) session.getAttribute("user")).getRole() == Role.ADMIN) {
				mav = new ModelAndView("redirect:/admin/home");
			} else {
				mav = new ModelAndView("redirect:/user/home");
			}
		}
		return mav;
	}

	@PostMapping(value = "/loginProcess")
	public String loginProcess(@ModelAttribute("login") User login, HttpSession session, Model model) {
		String mav = null;
		String username = login.getUsername();
		String password = DigestUtils.sha256Hex(login.getPassword());
		User user = userServices.findByUsernameAndPassword(username, password);
		if (user != null  && !(user.getRole() == Role.DELETED)) {
			session.setAttribute("user", user);
			if (user.getRole() == Role.ADMIN) {
				mav = "redirect:/admin/home";
			} else {
				mav = "redirect:/user/home";
			}
		} else {
			model.addAttribute("error", true);
			mav = "login-page";
		}
		return mav;
	}

	@RequestMapping("/logoutProcess")
	public String logoutProcess(HttpServletRequest session) {
		session.getSession().removeAttribute("user");
		return "redirect:/";
	}


}