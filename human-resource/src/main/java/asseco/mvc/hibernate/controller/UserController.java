package asseco.mvc.hibernate.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import asseco.mvc.hibernate.model.User;
import asseco.mvc.hibernate.service.UserServices;
import asseco.mvc.hibernate.util.Role;

@Controller
public class UserController {

	@Autowired
	private UserServices userService;

	@GetMapping("/admin/adduser")
	public String addUser(Model model, HttpServletRequest request) {
		model.addAttribute("newuser", new User());
		return "new-user";
	}

	@GetMapping("/admin/processDeleteUser")
	public String processDeleteUser(@RequestParam("userId") long id, HttpServletRequest request) {
		User user = userService.findById(id);
		user.setRole(Role.DELETED);
		userService.save(user);
		return "redirect:/admin/users";
	}

	@PostMapping("/admin/processAddUser")
	public String processAddUser(@Valid @ModelAttribute("newuser") User user,BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			return "new-user";
		}
		String username = user.getUsername();
		String password = DigestUtils.sha256Hex(user.getPassword());
		user.setUsername(username);
		user.setPassword(password);
		user.setRole(Role.USER);
		userService.save(user);
		return "redirect:/admin/users";
	}

	@GetMapping("admin/users")
	public String adminPayments(Model model, HttpServletRequest request) {
		model.addAttribute("payments", userService.findByRole(Role.USER));
		return "users";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
		dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
	}
}
