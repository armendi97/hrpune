package asseco.mvc.hibernate.controller;

import java.text.DecimalFormat;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import asseco.mvc.hibernate.model.Payment;
import asseco.mvc.hibernate.model.User;
import asseco.mvc.hibernate.service.PaymentService;
import asseco.mvc.hibernate.service.TransactionService;
import asseco.mvc.hibernate.service.UserServices;
import asseco.mvc.hibernate.util.PaymentStatus;
import asseco.mvc.hibernate.util.Role;

@Controller
@RequestMapping("/admin")
public class PaymentController {

	@Autowired
	private PaymentService paymentService;
	@Autowired
	private UserServices userService;
	@Autowired
	private TransactionService transactionService;
	
	@GetMapping("/home")
	public String adminPage(HttpServletRequest request) {
		return "admin-dashboard";
	}
	
	

	@GetMapping("/payments")
	public String adminPayments(Model model, HttpServletRequest request) {
//		String username = (String)request.getParameter("username");
//		String amount = (String)request.getParameter("amount");
//		String paymentDescription = (String)request.getParameter("paymentDescription");
//		String status = (String)request.getParameter("status");
//		
//		if(Util.isEmpty(username) && Util.isEmpty(amount) && Util.isEmpty(paymentDescription) && Util.isEmpty(status)) {
			model.addAttribute("payments", paymentService.findByOwnerOrderByStatusDesc((User) request.getSession().getAttribute("user")));		
//		}else {
//			model.addAttribute("payments", paymentDAO.search((User) request.getSession().getAttribute("user"),paymentDescription,PaymentStatus.present(status),username,Util.getDoubleValue(amount)));
//			System.out.println("pe kalon");
//		}
		return "admin-payments";
	}

	@GetMapping("/addpayment")
	public String addPayment(Model model, HttpServletRequest request) {
		model.addAttribute("newpayment", new Payment());
		model.addAttribute("users", userService.findByRole(Role.USER));
		model.addAttribute("pending", PaymentStatus.PENDING);
		return "add-payment";
	}

	@PostMapping("/processAddPayment")
	public String processAddPayment(@ModelAttribute("newpayment") Payment payment,
			@RequestParam("username") String username, HttpServletRequest request, Model model) {
		if (username == null || username.isEmpty() || username.equals("none")) {
			model.addAttribute("users", userService.findByRole(Role.USER));
			model.addAttribute("error", "Please choose user");
			return "add-payment";
		}
		payment.setUser(userService.findByUsername(username));

		payment.setOwner((User) request.getSession().getAttribute("user"));

		// change amount format
		DecimalFormat format = new DecimalFormat("0.00");
		payment.setAmount(Double.parseDouble(format.format(payment.getAmount())));

		payment.setStatus(PaymentStatus.PENDING);
		paymentService.save(payment);
		return "redirect:/admin/payments";
	}

	@GetMapping("/deletePayment")
	public String deletePayment(@RequestParam("paymentId") long id, HttpServletRequest request) {
		Payment payment = paymentService.findById(id);
		payment.setStatus(PaymentStatus.DELETED);
		paymentService.save(payment);
		return "redirect:/admin/payments";
	}

	@GetMapping("/editPayment")
	public String editPayment(@RequestParam("paymentId") long id, Model model, HttpServletRequest request) {
		Payment payment = paymentService.findById(id);
		model.addAttribute("editpayment", payment);
		model.addAttribute("users", userService.findByRole(Role.USER));
		return "edit-payment";
	}

	@PostMapping("/processEditPayment")
	public String procesEditPayment(@ModelAttribute("editpayment") Payment payment, HttpServletRequest request,
			@RequestParam("username") String username) {
		User pymentUser = userService.findByUsername(username);
		payment.setUser(pymentUser);
		payment.setOwner((User) request.getSession().getAttribute("user"));
		paymentService.save(payment);
		return "redirect:/admin/payments";
	}
	
	@GetMapping("/transactions")
	public String transactions(HttpServletRequest request,Model model) {
		model.addAttribute("transactions" , transactionService.findByPayment_owner((User)request.getAttribute("user")));
		return "admin-transactions";
	}

}
