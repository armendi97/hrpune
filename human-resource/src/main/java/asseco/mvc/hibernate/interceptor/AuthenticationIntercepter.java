package asseco.mvc.hibernate.interceptor;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import asseco.mvc.hibernate.model.User;

@Component
public class AuthenticationIntercepter implements HandlerInterceptor{

	@Override
	public  boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		User user =(User) request.getSession().getAttribute("user");
		if(user == null) {
			response.sendRedirect(request.getContextPath() + "/");	
			return false;
		}
		request.setAttribute("user", user);
		return true;
	}

	
}
