package asseco.mvc.hibernate.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import asseco.mvc.hibernate.model.User;
import asseco.mvc.hibernate.util.Role;

@Component
public class AdminAuthenticationIntercepter implements HandlerInterceptor {


	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		User user =(User) request.getSession().getAttribute("user");
		if(user.getRole() == Role.USER) {
			response.sendRedirect(request.getContextPath() + "/user/home");	
			return false;
		}
		request.setAttribute("user",user);
		return true;
	}
	
}
