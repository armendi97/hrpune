package asseco.mvc.hibernate.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import asseco.mvc.hibernate.dao.TransactionDAO;
import asseco.mvc.hibernate.model.Transaction;
import asseco.mvc.hibernate.model.User;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

	
	@Autowired
	private TransactionDAO transactionDAO;
	
	@Override
	public List<Transaction> getTransactions() {
		return transactionDAO.findAll();
	}

	@Override
	public void addTransaction(Transaction newTransaction) {
		transactionDAO.save(newTransaction);
	}

	@Override
	public Transaction getTransactionById(Integer id) {
		return transactionDAO.findById(id).orElse(null);
	}

	@Override
	public void deleteTransaction(Transaction transaction) {
		transactionDAO.delete(transaction);
		
	}

	@Override
	public List<Transaction> findByPayment_owner(User user) {
		return transactionDAO.findByPayment_owner(user);
	}

	
}
