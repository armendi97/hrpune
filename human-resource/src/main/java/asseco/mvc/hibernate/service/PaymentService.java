package asseco.mvc.hibernate.service;

import java.util.List;

import asseco.mvc.hibernate.model.Payment;
import asseco.mvc.hibernate.model.User;
import asseco.mvc.hibernate.util.PaymentStatus;
import asseco.mvc.hibernate.util.Role;

public interface PaymentService{

	List<Payment> findAll();
	void save(Payment entity);
	Payment findById(Long id) ;
	void delete(Payment entity);
	List<Payment> findByOwner(User user);
	List<Payment> findByUser(User user);
	List<Payment> findByUserAndStatus(User user , PaymentStatus paymentStatus);
	List<Payment> findByOwnerOrderByStatusDesc(User user);

}
