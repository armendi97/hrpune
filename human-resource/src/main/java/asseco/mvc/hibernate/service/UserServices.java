package asseco.mvc.hibernate.service;

import java.util.List;


import asseco.mvc.hibernate.model.User;
import asseco.mvc.hibernate.util.Role;

public interface UserServices {

	User findByUsernameAndPassword(String username, String password);
	List<User> findAll();
	void save(User entity);
	User findById(Long id) ;
	List<User> findByRole(Role role);
	User findByUsername(String username);
}
