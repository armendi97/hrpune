package asseco.mvc.hibernate.service;

import asseco.mvc.hibernate.model.User;
import asseco.mvc.hibernate.util.Role;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import asseco.mvc.hibernate.dao.UserDAO;

@Service
public class UserServicesImpl implements UserServices {
	@Autowired
	private UserDAO userDAO;
	
	@Override
	public User findByUsernameAndPassword(String username, String password) {
		return userDAO.findByUsernameAndPassword(username, password);
	}
	
	@Override
	public List<User> findAll() {
		return userDAO.findAll();
	}

	@Override
	@Transactional
	public void save(User entity) {
		userDAO.save(entity);
	}

	@Override
	public User findById(Long id) {
		return userDAO.findById(id).orElse(null);
	}

	@Override
	public List<User> findByRole(Role role) {
		return userDAO.findByRole(role);
	}

	@Override
	public User findByUsername(String username) {
		return userDAO.findByUsername(username);
	}

}
