package asseco.mvc.hibernate.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import asseco.mvc.hibernate.dao.PaymentDAO;
import asseco.mvc.hibernate.model.Payment;
import asseco.mvc.hibernate.model.User;
import asseco.mvc.hibernate.util.PaymentStatus;
import asseco.mvc.hibernate.util.Role;

@Service
@Transactional
public class PaymentServiceImpl implements PaymentService{

	@Autowired
	private PaymentDAO paymentDAO;
	
	@Override
	public List<Payment> findAll() {
		return paymentDAO.findAll();
	}

	@Override
	public void save(Payment entity) {
		paymentDAO.save(entity);
	}

	@Override
	public Payment findById(Long id) {
		return paymentDAO.findById(id).orElse(null);
	}

	@Override
	public void delete(Payment entity) {
		paymentDAO.delete(entity);
	}


	public List<Payment> findByOwner(User user) {
		return paymentDAO.findByOwner(user);

	}

	@Override
	public List<Payment> findByUser(User user) {
		return paymentDAO.findByUser(user);
	}

	@Override
	public List<Payment> findByUserAndStatus(User user, PaymentStatus paymentStatus) {
		return paymentDAO.findByUserAndStatus(user, paymentStatus);
	}

	@Override
	public List<Payment> findByOwnerOrderByStatusDesc(User user) {
		return paymentDAO.findByOwnerOrderByStatusDesc(user);
	}

}
