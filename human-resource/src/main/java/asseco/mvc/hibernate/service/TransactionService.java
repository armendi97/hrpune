package asseco.mvc.hibernate.service;

import java.util.List;

import asseco.mvc.hibernate.model.Transaction;
import asseco.mvc.hibernate.model.User;

public interface TransactionService {
	
	List<Transaction> getTransactions();
	void addTransaction(Transaction newTransaction);
	Transaction getTransactionById(Integer id);
	void deleteTransaction(Transaction transaction);
	List<Transaction> findByPayment_owner(User user);
}
