package asseco.mvc.hibernate.util;

public enum TransactionStatus {
	PENDING,
	DENIED,
	APPROVED;
}
