package asseco.mvc.hibernate.util;

public enum Role {
	ADMIN,
	USER,
	DELETED;
}
