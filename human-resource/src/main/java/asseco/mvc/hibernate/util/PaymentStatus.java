package asseco.mvc.hibernate.util;

public enum PaymentStatus {
	PENDING,
	APROVED,
	DENIED,
	DECLINED,
	DELETED;
	
	public static PaymentStatus present(String string) {
		switch(string.toUpperCase()) {
			case"PENDING":
				return PENDING;
			case"APROVED":
				return APROVED;
			case"DENIED":
				return DENIED;
			case"DECLINED":
				return DECLINED;
			case"DELETED":
				return DELETED;
			default:
				return null;
		}
	}
	
}
