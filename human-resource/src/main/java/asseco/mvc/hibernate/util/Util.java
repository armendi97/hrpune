package asseco.mvc.hibernate.util;

public class Util {	

	public static boolean isEmpty(String string) {
		return string == null || string.trim().isEmpty();
	}
	
	public static Double getDoubleValue(String string) {
		try {
			Double d = Double.parseDouble(string);
			return d;
		} catch (Exception e) {
			return new Double(0);
		}
	}
}
