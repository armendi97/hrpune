package asseco.mvc.hibernate.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import asseco.mvc.hibernate.util.TransactionStatus;

@Entity
@Table(name="aem_transaction")
public class Transaction {
	
	@Id
	@GeneratedValue
	@Column(name = "transaction_id")
	private int transactionId;
	
	@Column(nullable = false,name="transaction_date")
	@Temporal(TemporalType.DATE)
	private Date transactionDate;
	
	@Enumerated(EnumType.STRING)
	@Column(nullable = false,name="transaction_status")
	private TransactionStatus transactionStatus;
	
	@ManyToOne
	@JoinColumn(name="fk_payment_id")
	private Payment payment;

	public Transaction(int transactionId, Date transactionDate, TransactionStatus transactionStatus, Payment payment) {
		this.transactionId = transactionId;
		this.transactionDate = transactionDate;
		this.transactionStatus = transactionStatus;
		this.payment = payment;
	}
	
	public Transaction() {
		
	}

	public int getId() {
		return transactionId;
	}

	public void setId(int transactionId) {
		this.transactionId = transactionId;
	}

	public Date getDate() {
		return transactionDate;
	}

	public void setDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public TransactionStatus getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(TransactionStatus transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public final int getTransactionId() {
		return transactionId;
	}

	public final void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public final Date getTransactionDate() {
		return transactionDate;
	}

	public final void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	} 
	
}

