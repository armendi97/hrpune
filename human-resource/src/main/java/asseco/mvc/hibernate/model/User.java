package asseco.mvc.hibernate.model;

import java.util.List;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import asseco.mvc.hibernate.util.Role;
import javax.annotation.Generated;
import java.util.Collections;

@Entity
@Table(name ="aem_user")
public class User {
	
	@Id
	@GeneratedValue
	@Column(name= "user_id")
	private long userId;
	
	@Column(nullable = false, unique = true)
	@NotNull(message = "Username cannot be empty")
	@Size(min = 5 , message = "Username size should be at least 5 characters")
	private String username;
	
	@Column(nullable = false)
	@NotNull(message = "Password cannot be empty")
	private String password;
	
	@Column(nullable = false, unique = true)
	@Pattern(regexp = "[a-zA-Z]([a-zA-Z\\.\\-\\_]*[0-9]*)+@hotmail.com" , message = "Email should be of format [a-zA-Z][0-9](.)(-)(_)*@hotmail.com")
	@NotNull(message = "Email cannot be empty")
	private String email;
	
	@Column(name = "user_role" ,nullable = false)
	@Enumerated(EnumType.STRING)
	private Role role;
	
	@OneToMany(mappedBy = "user")
	private List<Payment> payment;

	@Generated("SparkTools")
	private User(Builder builder) {
		this.userId = builder.userId;
		this.username = builder.username;
		this.password = builder.password;
		this.email = builder.email;
		this.role = builder.role;
		this.payment = builder.payment;
	}

	public User(String username, String passwrod, String email, Role role, List<Payment> payment) {
		this.username = username;
		this.password = passwrod;
		this.email = email;
		this.role = role;
		this.payment = payment;
	}
	
	public User(){
		
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Payment> getPayment() {
		return payment;
	}

	public void setPayment(List<Payment> payment) {
		this.payment = payment;
	}

	@Override
	public String toString() {
		return username;
	}

	/**
	 * Creates builder to build {@link User}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link User}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private long userId;
		private String username;
		private String password;
		private String email;
		private Role role;
		private List<Payment> payment = Collections.emptyList();

		private Builder() {
		}

		public Builder withUserId(long userId) {
			this.userId = userId;
			return this;
		}

		public Builder withUsername(String username) {
			this.username = username;
			return this;
		}

		public Builder withPassword(String password) {
			this.password = password;
			return this;
		}

		public Builder withEmail(String email) {
			this.email = email;
			return this;
		}

		public Builder withRole(Role role) {
			this.role = role;
			return this;
		}

		public Builder withPayment(List<Payment> payment) {
			this.payment = payment;
			return this;
		}

		public User build() {
			return new User(this);
		}
	}
	
	

	
}
