package asseco.mvc.hibernate.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import asseco.mvc.hibernate.util.PaymentStatus;
import asseco.mvc.hibernate.util.Role;

@Entity
@Table(name = "aem_payment")
public class Payment {

	@Id
	@GeneratedValue
	@Column(name = "payment_id")
	public long paymentId;

	@Column(nullable = false, name = "payment_description")
	public String paymentDescription;

	@Column(nullable = false)
	public double amount;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private PaymentStatus status;

	@ManyToOne
	@JoinColumn(name = "fk_owner_id")
	private User owner;

	@ManyToOne
	@JoinColumn(name = "fk_user_id")
	private User user;
	
	@OneToMany(mappedBy = "payment",fetch = FetchType.LAZY)
	private List<Transaction> transactions;

	public Payment(int paymentId, String paymentDescription, double amount, PaymentStatus status, User owner, User user,
			List<Transaction> transactions) {
		this.paymentId = paymentId;
		this.paymentDescription = paymentDescription;
		this.amount = amount;
		this.status = status;
		this.owner = owner;
		this.user = user;
		this.transactions = transactions;
	}

	public Payment(long paymentId, String paymentDescription, double amount, PaymentStatus status, User owner,
			User user) {
		this.paymentId = paymentId;
		this.paymentDescription = paymentDescription;
		this.amount = amount;
		this.status = status;
		this.owner = owner;
		this.user = user;
	}

	public Payment() {

	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public PaymentStatus getStatus() {
		return status;
	}

	public void setStatus(PaymentStatus status) {
		this.status = status;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	public static void main(String[] args) {
		User user = User.builder().withEmail("test@email.com").withRole(Role.ADMIN).build();
	}
}
