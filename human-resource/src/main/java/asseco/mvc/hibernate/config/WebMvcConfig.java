package asseco.mvc.hibernate.config;

import org.aopalliance.intercept.Interceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import asseco.mvc.hibernate.interceptor.AdminAuthenticationIntercepter;
import asseco.mvc.hibernate.interceptor.AuthenticationIntercepter;
import asseco.mvc.hibernate.interceptor.UserAuthenticationIntercepter;

@EnableWebMvc
@Configuration
@EnableJpaRepositories("asseco.mvc.hibernate.dao")
@ComponentScan(basePackages = {"asseco.mvc.hibernate"})
public class WebMvcConfig implements WebMvcConfigurer{
	
	@Autowired 
	private AuthenticationIntercepter autheHandlerInterceptor;
	@Autowired
	private AdminAuthenticationIntercepter adminAutheHandlerInterceptor;
	@Autowired
	private UserAuthenticationIntercepter userAuthenticationIntercepter;
	
	@Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }
	
	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
	    registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(autheHandlerInterceptor).excludePathPatterns("/","/login","/loginProcess");
		registry.addInterceptor(adminAutheHandlerInterceptor).addPathPatterns("/admin/**");
		registry.addInterceptor(userAuthenticationIntercepter).addPathPatterns("/user/**");
	}
	


	
}