package asseco.mvc.hibernate.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import asseco.mvc.hibernate.model.Transaction;
import asseco.mvc.hibernate.model.User;

@Repository
public interface TransactionDAO extends JpaRepository<Transaction, Integer>{
	
	List<Transaction> findByPayment_owner(User user);
}
