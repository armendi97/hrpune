package asseco.mvc.hibernate.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import asseco.mvc.hibernate.model.Payment;
import asseco.mvc.hibernate.model.User;
import asseco.mvc.hibernate.util.PaymentStatus;

@Repository
public interface PaymentDAO extends JpaRepository<Payment, Long>{

	
	List<Payment> findByOwner(User user);
	List<Payment> findByUser(User user);
	List<Payment> findByUserAndStatus(User user , PaymentStatus paymentStatus);
	List<Payment> findByOwnerOrderByStatusDesc(User user);
//	@Query("FROM Payment AS p LEFT JOIN p.owner AS owner LEFT JOIN p.user as user "
//			+ "WHERE "
//			+ "		((p.paymentDescription LIKE CASE WHEN ?#{#paymentDescription} IS NULL THEN %''% ELSE %?#{#paymentDescription}% END))")
//			+ "	AND "
//			+ "		(p.status in"
//			+ "			CASE "
//			+ "				WHEN ?#{#status} IS NULL THEN"
//			+ "				 	('PENDING','DELETED','DECLINED','APROVED','DENIED') "
//			+ "				ELSE"
//			+ "					 (?#{#status})"
//			+ "				END)"
//			+ "AND	(user.username LIKE"
//			+ "			CASE "
//			+ "				WHEN ?#{#username} IS NULL THEN  "
//			+ "					'%%'"
//			+ "				ELSE"
//			+ "					%?#{#username}%)"
//			+ "				END "
//			+ "AND"
//			+ "		 (p.amount => "
//			+ "			Case "
//			+ "				WHEN ?#{#amount} = 0 THEN"
//			+ "					0"
//			+ "				ELSE"
//			+ "					#{#amount}"
//			+ "				END) "
//			+ "AND (p.owner = ?#{#user})) "
//			+ "ORDER BY p.status DESC")
//	List<Payment> search(@Param("user")User user ,
//			@Param("paymentDescription") String paymentDescription,
//			@Param("status") PaymentStatus status,
//			@Param("username") String username,
//			@Param("amount") double amount);


}
