package asseco.mvc.hibernate.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import asseco.mvc.hibernate.model.User;
import asseco.mvc.hibernate.util.Role;

@Repository
public interface UserDAO extends JpaRepository<User, Long>{
	User findByUsernameAndPassword(String username, String password);
	List<User> findByRole(Role role);
	User findByUsername(String username);

}